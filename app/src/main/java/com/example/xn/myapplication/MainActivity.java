package com.example.xn.myapplication;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btn,insertBtn;
    private EditText nameEdit;

    private MyDatabaseOpenHelper helper;
    private SQLiteDatabase db;
    private static final String TABLE_NAME = "cdinfo";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindID();


        helper = new MyDatabaseOpenHelper(this, "dbName", null, 1);//dbName数据库名



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                db = helper.getWritableDatabase();
            }
        });

        insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db = helper.getWritableDatabase();

                ContentValues values = new ContentValues();

                String nameEd = nameEdit.getText().toString();
                values.put("id", 1);
                values.put("type", 0);

                db.insert(TABLE_NAME, null, values);
                values.clear();
                Toast.makeText(MainActivity.this, "插入" + nameEd + "成功", Toast.LENGTH_SHORT).show();

                db.close();
            }
        });
    }
        private void bindID() {
            btn = (Button) findViewById(R.id.create_btn);
            insertBtn = (Button) findViewById(R.id.insert_btn);
            nameEdit = (EditText) findViewById(R.id.name_edit);

        }

    }

