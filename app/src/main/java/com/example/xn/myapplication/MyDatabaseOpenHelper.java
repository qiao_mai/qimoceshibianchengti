package com.example.xn.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by XN on 2020/7/3.
 */

public class MyDatabaseOpenHelper extends SQLiteOpenHelper {

     private Context context;

   public static final String cdinfo = "create table cdinfo ("
           + "id integer primary key autoincrement," + "id"
            + "type)";


    public MyDatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {

        super(context, "finaldb", null, 1);
        this.context = context;
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(cdinfo);
        Toast.makeText(context,"Create succeeded",Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}


